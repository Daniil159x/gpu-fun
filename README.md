# GPU Fun

**gpu Fun** - project for an exercise in programming for gpu.  

## Work

This project implement the conversion RGB image to grayscale and scaling it by 2 times.

Example image locate in `samples` folder.

## Hardware

The follow hardware is used for OpenCL:  
CPU: AMD Ryzen 5 3600
GPU: AMD Radeon RX 5700 XT

## Result

Only the running time on the GPU was taken into account.  
Time is milliseconds.

### OpenCl

|        File          | Avg time |  Total time |
|----------------------|----------|-------------|
| Lenna_1024_768.png   |    57.46 |        862  |
| Lenna_1280_960.png   |    59.93 |        899  |
| Lenna_2048_1536.png  |    63.6  |        954  |
| Lenna_2048_2048.png  |    65.0  |        975  |
| Lenna_8192_8192.png  |    87.93 |       1319  |
