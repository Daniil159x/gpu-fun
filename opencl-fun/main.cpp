#include "pch.hpp"

using namespace std::string_literals;

void work(const char * inFilename, const char * outFilename)
{
    printMillisecond("begin");

    std::vector<unsigned char> rgbImage;
    unsigned int width = 0, height = 0;
    int err = lodepng::decode(rgbImage, width, height, inFilename, LCT_RGB);
    if (err)
    {
        throw std::runtime_error("Open file error: "s + lodepng_error_text(err));
    }

    auto [context, device] = DeviceContext::getDefaultGPU();

    unsigned int outWidth = width * 2;
    unsigned int outHeight = height * 2;
    std::vector<unsigned char> grayscaleImg(outWidth * outHeight);

    printMillisecond("image is read");

    auto buffRgbImg = cl::Buffer(context,
                                 CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                 rgbImage.size(),
                                 rgbImage.data());

    auto buffOutImg = cl::Buffer(context,
                                 CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
                                 grayscaleImg.size(),
                                 grayscaleImg.data());

    printMillisecond("buffers are ready");

    cl::Program program(context, Programs::grayScale);
    program.build({ device });

    cl::Kernel kernel(program, "scalingGrayScale");
    kernel.setArg(0, buffRgbImg);
    kernel.setArg(1, buffOutImg);

    printMillisecond("kernel is compiled");

    cl::CommandQueue queue(context, device, 0);
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(width, height));
    queue.finish();

    printMillisecond("queue done");

    queue.enqueueReadBuffer(buffOutImg, true, 0, grayscaleImg.size(), grayscaleImg.data());

    printMillisecond("result is copied");

    err = lodepng::encode(outFilename, grayscaleImg, outWidth, outHeight, LCT_GREY);
    if (err)
    {
        throw std::runtime_error("Saving file error: "s + lodepng_error_text(err));
    }

    printMillisecond("saved");
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Usage: " << argv[0] << " rgbImageIn grayscaleImageOut" << std::endl;
        return -1;
    }

    try
    {
//        printOpenCL();
        work(argv[1], argv[2]);
    }
    catch (cl::BuildError const &e)
    {
        std::cerr << "Build OpenCl error: " << e.what() << std::endl;
        for(auto &[dev, log] : e.getBuildLog())
        {
            std::cerr << dev.getInfo<CL_DEVICE_NAME>() << std::endl;
            std::cerr << log << std::endl;
        }
        return -1;
    }
    catch (cl::Error const &e)
    {
        std::cerr << "OpenCl exception with code = " << e.err() << " :" << e.what() << std::endl;
        return -1;
    }
    catch (std::exception const &e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
