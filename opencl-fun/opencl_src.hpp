#ifndef OPENCL_FUN_OPENCL_SRC_HPP
#define OPENCL_FUN_OPENCL_SRC_HPP

#define STRINGIFY(x) #x

namespace Programs
{
    static constexpr const char *grayScale = STRINGIFY(
        kernel void scalingGrayScale(global unsigned char *grbImage,
                                     global unsigned char *grayImage) {
            size_t x = get_global_id(0);
            size_t y = get_global_id(1);
            size_t w = get_global_size(0);

            size_t index         = y * w + x;
            size_t outIndexLine1 = y * 2 * w * 2 + x * 2;
            size_t outIndexLine2 = (y * 2 + 1) * w * 2 + x * 2;

            int pixel = grbImage[index * 3];  // R
            pixel += grbImage[index * 3 + 1]; // G
            pixel += grbImage[index * 3 + 2]; // B
            pixel /= 3;

            grayImage[outIndexLine1 + 0]     = pixel;
            grayImage[outIndexLine1 + 1] = pixel;
            grayImage[outIndexLine2 + 0]     = pixel;
            grayImage[outIndexLine2 + 1] = pixel;
        }

        kernel void drawWorkGroup(global unsigned char *grbImage, global unsigned char *grayImage) {
            size_t x = get_global_id(0);
            size_t y = get_global_id(1);
            size_t w = get_global_size(0);

            size_t index = y * w + x;

            grayImage[index] = 255 - get_local_id(0) * get_local_id(1);
        });
}

#endif //OPENCL_FUN_OPENCL_SRC_HPP
