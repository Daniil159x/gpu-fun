#ifndef OPENCL_FUN_PCH_HPP
#define OPENCL_FUN_PCH_HPP

#include <iostream>

#include <CL/cl2.hpp>
#include <lodepng.h>

#include "utils.hpp"

#include "opencl_src.hpp"

#endif //OPENCL_FUN_PCH_HPP
