#include "pch.hpp"
#include "utils.hpp"


DeviceContext DeviceContext::getDefaultGPU()
{
    auto platform = cl::Platform::getDefault();

    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

    if (devices.empty())
    {
        throw std::runtime_error("Default gpu device not found");
    }

    std::array<cl_context_properties, 3> props
        = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform(), 0 };

    cl::Context context(devices[0], props.data());

    return { context, devices[0] };
}

std::string deviceTypeToStr(cl_device_type type)
{
    std::string res;

    if (type & CL_DEVICE_TYPE_GPU)
    {
        res += "GPU";
    }

    if (type & CL_DEVICE_TYPE_CPU)
    {
        res += "CPU";
    }

    if (res.empty())
    {
        res = std::to_string(type);
    }

    return res;
}

void printOpenCL()
{
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    std::cout << "platforms:" << std::endl;
    for (size_t i = 0; i < platforms.size(); ++i)
    {
        std::cout << i << ":" << std::endl;
        printOpenCLPlatform(platforms[i], "  ");
    }
}

void printOpenCLPlatform(cl::Platform const &platform, std::string_view indent)
{
    auto name   = platform.getInfo<CL_PLATFORM_NAME>();
    auto vendor = platform.getInfo<CL_PLATFORM_VENDOR>();
    auto ver    = platform.getInfo<CL_PLATFORM_VERSION>();

    std::cout << indent << "name: " << name << std::endl;
    std::cout << indent << "vendor: " << vendor << std::endl;
    std::cout << indent << "version: " << ver << std::endl;

    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    std::string devicesIndent(indent);
    devicesIndent += indent;

    std::cout << indent << "devices:" << std::endl;
    for (size_t j = 0; j < devices.size(); ++j)
    {
        std::cout << indent << j << std::endl;
        printOpenClDevice(devices[j], devicesIndent);
    }
}

void printOpenClDevice(cl::Device const &device, std::string_view indent)
{
    auto dev_name = device.getInfo<CL_DEVICE_NAME>();
    auto dev_type = device.getInfo<CL_DEVICE_TYPE>();
    auto dev_ver  = device.getInfo<CL_DEVICE_VERSION>();

    std::cout << indent << "name: " << dev_name << std::endl;
    std::cout << indent << "type: " << deviceTypeToStr(dev_type) << std::endl;
    std::cout << indent << "version: " << dev_ver << std::endl;
}

void printMillisecond(std::string_view prefix)
{
    using namespace std::chrono;

    static auto oldNow = steady_clock::now();
    auto now = steady_clock::now();

    auto sinceEpoch = duration_cast<milliseconds>(now.time_since_epoch()).count();
    auto sinceOldNew = duration_cast<milliseconds>(now - oldNow).count();

    std::cout << prefix << " [" << sinceEpoch << "ms]: " << sinceOldNew << "ms" << std::endl;

    oldNow = now;
}
