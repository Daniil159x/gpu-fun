#ifndef OPENCL_FUN_UTILS_HPP
#define OPENCL_FUN_UTILS_HPP

struct DeviceContext
{
    cl::Context context;
    cl::Device device;

    static DeviceContext getDefaultGPU();
};

std::string deviceTypeToStr(cl_device_type type);

void printOpenCL();
void printOpenCLPlatform(cl::Platform const &platform, std::string_view indent = "");
void printOpenClDevice(cl::Device const &device, std::string_view indent = "");

void printMillisecond(std::string_view prefix);

#endif //OPENCL_FUN_UTILS_HPP
