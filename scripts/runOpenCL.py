#!/usr/bin/env python3
import os.path
import pathlib
import re
import sys
import subprocess

launchesNumber = 15

fileNames = (
    "Lenna_1024_768.png",
    "Lenna_1280_960.png",
    "Lenna_2048_1536.png",
    "Lenna_2048_2048.png",
    "Lenna_8192_8192.png",
)


def run(path_to_exec, path_to_image, path_to_result):
    args = [path_to_exec, str(path_to_image), str(path_to_result)]
    res = subprocess.run(args, capture_output=True)

    if res.returncode != 0:
        print(f"Launch '{args}' failed")
        print(res.stdout.decode())
        print(res.stderr.decode())
        raise Exception("Program returned a non-zero code")

    res = re.findall(r"([\w ]+) \[\d+ms\]: (\d+)ms", res.stdout.decode())
    res = dict(res)

    return int(res["queue done"])


def print_row(file, avg, total):
    print(f"| {file:20} | {avg:8} | {total:10} |")


if __name__ == "__main__":
    path_to_exec = sys.argv[1]
    path_to_samples = "../samples" if len(sys.argv) <= 2 else sys.argv[2]
    path_to_results = "../samples/res" if len(sys.argv) <= 3 else sys.argv[3]

    path_to_samples = pathlib.Path(path_to_samples)
    path_to_results = pathlib.Path(path_to_results)
    path_to_results.mkdir(parents=True, exist_ok=True)

    print_row("Filename", "Avg Time", "Total Time")
    for name in fileNames:
        path_to_image = path_to_samples / name
        if not path_to_image.exists():
            print(f"WARNING: file {path_to_image} not found, skip")
            continue

        path_to_result = path_to_results / name

        total_time = 0
        for i in range(launchesNumber):
            total_time += run(path_to_exec, path_to_image, path_to_result)

        avg_time = total_time / launchesNumber
        print_row(name, avg_time, total_time)
